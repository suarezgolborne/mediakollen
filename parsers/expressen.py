from baseparser import BaseParser
from BeautifulSoup import BeautifulSoup, Tag


class exParser(BaseParser):
    domains = ['www.expressen.se']
    
    feeder_pat   = '^http://www.expressen.se/'
    feeder_pages = ['http://www.expressen.se/']

    def _parse(self, html):
        soup = BeautifulSoup(html, convertEntities=BeautifulSoup.HTML_ENTITIES, fromEncoding='utf-8')

# self.meta = soup.findAll('meta')
        elt = soup.find('h1')
        if elt is None:
            self.real_article = False
            return
        self.title = elt.getText()
        self.byline = ''
        self.date = ''


        div = soup.find('div', 'b-text_article')
        if div is None:
            self.real_article = False
            return

        self.body = '\n'+'\n\n'.join([x.getText() for x in div.childGenerator()
        if isinstance(x, Tag) and x.name == 'p'])
