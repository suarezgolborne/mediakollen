from baseparser import BaseParser, grab_url, logger

# Different versions of BeautifulSoup have different properties.
# Some work with one site, some with another.
# This is BeautifulSoup 3.2.
from BeautifulSoup import BeautifulSoup
# This is BeautifulSoup 4
import bs4
import re

class SVTParser(BaseParser):
    domains = ['www.svt.se']

    feeder_pat   = '^http://www.svt.se/nyheter/'
    feeder_pages = ['http://www.svt.se/nyheter/']

    feeder_bs = bs4.BeautifulSoup

def _parse(self, html):
    """Retrieve and serve the required fields to create an entry."""
    soup = BeautifulSoup(html,
                         convertEntities=BeautifulSoup.HTML_ENTITIES,
                         fromEncoding='utf-8')
                         
    self.meta = soup.findAll('meta')
    self.title = soup.find('h1').getText()
    self.date = soup.find('div', 'article-date').next.next.getText()
    self.byline = soup.find('div', 'field-name-byline').next.next.next.getText()
    self.body = soup.find('div', 'field-name-body').getText()