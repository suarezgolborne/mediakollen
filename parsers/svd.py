from baseparser import BaseParser
from BeautifulSoup import BeautifulSoup, Tag


class SVDParser(BaseParser):
    domains = ['www.svd.se']
    
    feeder_pat   = '^http://www.svd.se/'
    feeder_pages = ['http://www.svd.se/']

    def _parse(self, html):
        soup = BeautifulSoup(html, convertEntities=BeautifulSoup.HTML_ENTITIES, fromEncoding='utf-8')

# self.meta = soup.findAll('meta')
        elt = soup.find('h1')
        if elt is None:
            self.real_article = False
            return
        self.title = elt.getText()
        self.byline = ''
        self.date = ''


        div = soup.find('div', 'articletext')
        if div is None:
            self.real_article = False
            return

        self.body = '\n'+'\n\n'.join([x.getText() for x in div.childGenerator()
        if isinstance(x, Tag) and x.name == 'p'])
