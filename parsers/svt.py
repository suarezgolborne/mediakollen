from baseparser import BaseParser
from BeautifulSoup import BeautifulSoup, Tag


class SVTParser(BaseParser):
    domains = ['www.svt.se']
    
    feeder_pat   = '^http://www.svt.se/nyheter/'
    feeder_pages = ['http://www.svt.se/nyheter/']

    def _parse(self, html):
        soup = BeautifulSoup(html, convertEntities=BeautifulSoup.HTML_ENTITIES, fromEncoding='utf-8')

# self.meta = soup.findAll('meta')
        elt = soup.find('h1', 'svtH1-THEMED')
        if elt is None:
            self.real_article = False
            return
        self.title = elt.getText()
        self.byline = ''
        self.date = ''



        div = soup.find('div', 'svtTextBread-Article')
        if div is None:
            self.real_article = False
            return

        self.body = '\n'+'\n\n'.join([x.getText() for x in div.childGenerator()
        if isinstance(x, Tag) and x.name == 'p'])
