#!/venv/bin/python2.7

import os
import sys
sys.path.append('/Users/carinajonsson/sebdev/svtdiffs/')
# sys.path.append('/Users/carinajonsson/sebdev/svtdiffs/website')
# sys.path.append('/Users/carinajonsson/sebdev/svtdiffs/website/frontend')



# Use dev settings if not otherwise configured.
THIS_DIR = os.path.dirname(os.path.abspath(__file__))
try:
    os.symlink('settings_dev.py', THIS_DIR+'/settings.py')
except OSError:
    pass

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "website.settings")
    sys.path.append(os.path.dirname(os.getcwd()))
    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
